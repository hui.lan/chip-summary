# python select_peaks.py a.narrowPeak p=0.05 q=0.01 fc=4
import sys
import os
import math

pvalue = 0.05
qvalue = 0.01
fc     = 2

count = 0


if len(sys.argv) < 2:
    sys.exit()

if not os.path.isfile(sys.argv[1]):
    print('No file %s' % (sys.argv[1]))
    sys.exit()

for x in sys.argv[1:]:
    if x.startswith('p='):
        pvalue = float(x[2:])
    if x.startswith('q='):
        qvalue = float(x[2:])
    if x.startswith('fc='):
        fc = float(x[3:])

p = -math.log10(pvalue)
q = -math.log10(qvalue)
f = open(sys.argv[1])
for line in f:
    line = line.strip()
    lst = line.split()
    fold_change = float(lst[6])
    neglog10pvalue = float(lst[7])
    neglog10qvalue = float(lst[8])
    if neglog10pvalue >= p and neglog10qvalue >= q and fold_change >= fc:
        print(line)
        count += 1
        
f.close()

#print('pvalue<%g, qvalue<%g,  fc>%d,  #peaks=%d' % (pvalue, qvalue, fc, count))
