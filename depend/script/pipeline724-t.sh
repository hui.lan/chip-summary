# NCIS
est_scale_factor="n"
est_extension_size="y"

# peak call parameters
pvalue="0.05"
qvalue="0"

if [ "$est_scale_factor" = "y" ]
then
    echo "Estimate scaling factor ..."    
    temp_name=$(cat /dev/urandom | tr -dc 'A-Z' | fold -w 6 | head -n 1)
    echo "$temp_name"
    echo "    Convert BAM files to BED files "
    bedtools bamtobed -i $chip > "$temp_name""_chip.bed"
    bedtools bamtobed -i $input > "$temp_name""_input.bed"
    echo "    Estimate (it may take one hour)"
    Rscript /home/hui/script/estimate_scaling_factor.r "$temp_name""_chip.bed" "$temp_name""_input.bed"
    ratio=$(cat scale-factor.txt)
    rm -f "${temp_name}*.bed"
fi


if [ "$est_extension_size" = "y" ]
then
    echo "Estimate fragment size ..."
    macs2 predictd -i $chip &> macs2-predictd.txt
    extsize=$(grep "predicted fragment length" macs2-predictd.txt  | grep -Po '[0-9]+ bps' | tr -d 'a-z')
    echo "    the estimated d is $extsize"
fi


echo "Save results in file names started with $name"
additional_arguments_for_macs2=" --nomodel "
if [ "$est_scale_factor" = "y" ]
then
    additional_arguments_for_macs2="$additional_arguments_for_macs2 --ratio $ratio"
fi

if [ "$pvalue" != "0" ]
then
    additional_arguments_for_macs2="$additional_arguments_for_macs2 -p $pvalue"
fi

if [ "$qvalue" != "0" ]
then
    additional_arguments_for_macs2="$additional_arguments_for_macs2 -q $qvalue"
fi

if [ "$est_extension_size" = "y" ]
then
    additional_arguments_for_macs2="$additional_arguments_for_macs2 --extsize $extsize"
fi

echo "Call peaks ..."
macs2 callpeak -t $chip -c $input -f BAM -g 121576530 --keep-dup 1 -n $name $additional_arguments_for_macs2
