import os, sys, operator

def get_go(file, category):

    d = {}
    f = open(file)
    for line in f:
        lst = line.split('\t')
        if line.startswith('GO:') and lst[1] == category.upper():
            d[lst[0]] = lst[3]
    f.close()
    return d

count_d = {}
big_d = {}
union_d = {}
names = []
col = 0
for f in sys.argv[1:]:
    dg = get_go(f, 'bp')
    name = os.path.splitext(os.path.basename(f))[0]
    names.append(name)
    big_d[name] = dg
    col += 1
    for go in dg:
        if not go in count_d:
            count_d[go] = (1, col)
        else:
            t = count_d[go]
            t = (t[0]+1, t[1] + col)
            count_d[go] = t
        if not go in union_d:
            union_d[go] = dg[go]
            
sorted_count_dict = sorted(count_d.items(), key=operator.itemgetter(1), reverse=True)

s = 'GO_term\t'
for n in names:
    s += n + '\t'
s += 'GO_term_description'
print(s)

for t in sorted_count_dict:
    go = t[0]
    count = t[1][0]
    go_description = union_d[go]
    s = go + '\t' + str(count)
    for n in names:
        if go in big_d[n]:
            s += '\t' + '1'
        else:
            s += '\t' + '0'
    s += '\t[' + go_description + ']'
    print(s)
