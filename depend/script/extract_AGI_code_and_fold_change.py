import codecs
import sys
f = codecs.open(sys.argv[1], 'r', 'utf-8')

L = []

d = {}


print('\t'.join(['AGI_code', 'max_fold_change_in_nearby_peaks']))
def splittedname(s):
    return tuple(int(x) for x in s.split('.'))
    
for line in f:
    lst = line.split()
    for x in lst:
        if x.lower().startswith('at') and len(x) >= 9 and x[2].isdigit():
            if lst[6].find('affects') != -1:
                print(line)
                sys.exit()
            gene = x[0:9].upper()
            if not gene in d:
                d[gene] = lst[6]
            else:
                d[gene] += '\t' + lst[6]

for x in sorted(d):
    s = '%s' % (x)
    s += '\t'
    value = d[x]
    l = sorted(value.split(), key=splittedname, reverse=True)
    s += l[0]
    print(s)

f.close()
