import sys
import os.path

def get_description(x, d):
        result = ''
	if 0 < len(x) <= 9:
		x = x + '.1' 
	if x in d:
	        result += '    ' + x + ':    ' + d[x]
	else:
		result += '    ' + x + ': Not Found'
        return result
        
locus_file = sys.argv[1]
f0 = open(sys.argv[2]) # gene description file, e.g., /media/pw_synology3/Software/chip-summary/depend/data/gene_description_20140101.txt

d = {}
for line in f0:
	line = line.strip()
	lst = line.split()
	id = lst[0]
	if not id in d:
		d[id] = line

f0.close()

if  not os.path.isfile(locus_file):  # gene names provided as a string
        line = locus_file
        line = line.replace('\"', '')
        lst = line.split(';')
        result = ''
        for x in lst:
                result += get_description(x.upper(), d)
        print(result)
else:
        f = open(locus_file)

        for line in f:
	        line = line.strip()
	        line = line.replace('\"', '')
	        lst = line.split(';')
	        result = ''
	        for x in lst:
                        result += get_description(x, d)
	        print(result)

        f.close()

