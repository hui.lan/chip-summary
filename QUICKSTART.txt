=============================
Chip-summary Quick Start
=============================

:Last modified: 2016-10-10, 2017-07-14, 2017-07-25

Chip-summary generates an html report summarising potential target genes for each mapped ChIP-seq sample (in BAM format).  In addition, it generates GO enrichment for these target genes, and can (optionally) make pairwise comparison between sets of target genes.


Prerequisites
================

chip-summary uses the following software:

- MACS2: a popular peak calling software

- bedmap

- goatools -- After installing goatools, you should update depend/script/fe.sh.  You should also download go-basic.obo [http://geneontology.org/ontology/go-basic.obo].

We should be able to run it on our server without modification.  Tested using my account and michelle's account.


Input
=======

summary.txt


Output
========

summary.html


Quick start
==============

Copy the entire chip-summary directory to your working folder.  Go to chip-summary.  Take a look at the given summary.txt.

Create your own summary.txt in that directory. In the command line, type

::

   python chip-summary.py summary.txt


Wait.


The most important step is to prepare a summary.txt,  a text file specifying parameters and file locations.  The parameter lines start with ``%%``, while file location lines start with ``@``.   Below is a list of acceptable parameters and their definition:

- FC: fold-change, used for selecting peaks returned from MACS2.
- QVALUE: q-value (FDR), used for selecting peaks returned from MACS2.
- PVALUE: p-value, selecting peaks returned from MACS2.
- TARGET_RANGE: number of base pairs from peak to inclue potential target genes. Default: 3000.
- TITLE: summary title, can be anything.
- PATH: path to BAM files.
- PAIRWISE_COMPARE: if equal to YES or Y, do pairwise comparison between ChIP-seq samples.

Each ChIP-seq sample starts with ``@``, followed by the ChIP-seq sample name (or whatever meaningful name).  Any meaningful name will work.  The second line starts with *CHIP:* and specifies the ChIP's location. The third line starts with *INPUT:* and specifies the INPUT's location.  A per-sample parameter for peak selecting can be specified like this: ``%FC=4.0 QVALUE=0.01 PVALUE=0.05``.  This will override the global parameter for peak selecting.

Example summary.txt:

::

	%%FC=3.0 QVALUE=0.001 PVALUE=0.05
	%%PATH=/media/pw_synology3/PW_HiSeq_data/ChIP-seq/Mapped_data/100C_LIB_23062015
	%%PAIRWISE_COMPARE=YES
	%%TARGET_RANGE=1000
	%%TITLE=PIF4 binding
	
	@WT-TP2-27-PIF4PIF4-FLAG_S24
	CHIP:<PATH>/TP2-27-PIF4PIF4-FLAG_S24/WTTP2-27-PIF4PIF4-FLAG_S24_raw_bowtie2_TAIR10_ensembl_nomixed_sorted_rmdup_picard.bam
	INPUT:/media/pw_synology3/PW_HiSeq_data/ChIP-seq/Mapped_data/2_84C/HSP70GFP/HSP70GFP-TP1-22-c-2_S24_raw_bowtie2_TAIR10_ensembl_nomixed_sorted_rmdup_picard.bam
	
	@WT-TP4-27-PIF4PIF4-FLAG_S15
	CHIP:<PATH>/TP4-27-PIF4PIF4-FLAG_S15/WTTP4-27-PIF4PIF4-FLAG_S15_raw_bowtie2_TAIR10_ensembl_nomixed_sorted_rmdup_picard.bam
	INPUT:/media/pw_synology3/PW_HiSeq_data/ChIP-seq/Mapped_data/2_84C/HSP70GFP/HSP70GFP-TP1-22-c-2_S24_raw_bowtie2_TAIR10_ensembl_nomixed_sorted_rmdup_picard.bam


Tips
=====

After first run, you may change FC, QVALUE, PVALUE, etc.  To avoid calling peaks again, comment out the three lines after ‘#DON’T RUN AGAIN’ in chip-summary.py.

To let chip-summary.py run anywhere, make sure DEPENDENT_FILES_PATH is set to the path to chip-summary.  Then you can copy chip-summary.py to anywhere and run it there.


Questions
==========

Contact Hui.



