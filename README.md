# Chip-summary

Chip-summary generates an html report summarising target genes for each mapped ChIP-seq sample (in BAM format).  In addition, it generates GO enrichment for these target genes, and can (optionally) make pairwise comparison between sets of target genes.

Specify BAM file paths in summary.txt. See the given summary.txt for an example.

Type the following command:  python chip-summary.py summary.txt

Check summary.html.

See QUICKSTART.txt for more information.
